import {NgModule} from '@angular/core';
import {NbMenuModule} from '@nebular/theme';

import {ThemeModule} from '../@theme/theme.module';
import {PagesComponent} from './pages.component';
import {HttpClient} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {PortletModule} from "../shares/portlet/portlet.module";
import {TooltipModule} from "primeng/tooltip";
import {TableModule} from "primeng/table";
import {InputTextModule} from "primeng/inputtext";
import { RouterModule, Routes } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


// AOT compilation support
export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [

    {
      path: 'teacher',
      loadChildren: () => import('./teachers/teachers.module').then(m => m.TeachersModule),
    },
    {
      path: 'student',
      loadChildren: () => import('./students/students.module').then(m => m.StudentsModule),
    },
    {
      path: 'room',
      loadChildren: () => import('./rooms/rooms.module').then(m => m.RoomsModule),
    },

    {
      path: '**',
      redirectTo: 'error/404',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        ThemeModule,
        NbMenuModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: httpTranslateLoader,
                deps: [HttpClient],
            },
        }),
        PortletModule,
        TooltipModule,
        TableModule,
        InputTextModule,
        FormsModule,
        ReactiveFormsModule

    ],
  declarations: [
    PagesComponent
  ],
})
export class PagesModule {
}
