import {NbMenuItem} from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'home-outline',
    link: '/pages/dashboard',
  },
  // {
  //   title: 'Users',
  //   icon: 'person-outline',
  //   link: '/pages/users',
  // },
  {
    title: 'Teacher',
    icon: 'layers-outline',
    link: '/pages/teacher',
  },
  {
    title: 'Student',
    icon: 'shopping-bag-outline',
    link: '/pages/student',
  },
  {
    title: 'Room',
    icon: 'home-outline',
    link: '/pages/room',
  },
  // {
  //   title: 'Order',
  //   icon: 'shopping-cart-outline',
  //   link: '/pages/order',
  // },



];
