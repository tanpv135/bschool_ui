import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService, MessageService, PrimeNGConfig } from 'primeng/api';
import { DEFAULT_MODAL_OPTIONS } from '../../@core/app-config';
import { ActionRoomComponent } from './action-room/action-room.component';
import { Rooms } from './Rooms';
import { RoomsService } from './rooms.service';

class PagedData<T> {
  data: T[];
}

@Component({
  selector: 'ngx-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit {

  roomDialog: boolean;

  rooms: Rooms[];


  room: Rooms;

  selectedRooms: Rooms[];

  submitted: boolean;

  constructor(
    private roomService: RoomsService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private fb : FormBuilder,
    private primengConfig: PrimeNGConfig,
    private toastr: ToastrService,
    private modal: NgbModal
  ) { }

  ngOnInit(): void {
    this.getAll();
    this.primengConfig.ripple = true;
  }

  public getAll():void{
    this.roomService.getALlRoom().subscribe(
      data =>{
        this.rooms = data;
      }, error => console.log(error)
    );
  }

  processEdit(room: any) {
    const modalRef = this.modal.open(ActionRoomComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance.action = false;
    modalRef.componentInstance.room = room;
    modalRef.result.then(value => {
        if (value) {
          this.getAll();
        }
      },
    );
  }

  processSave($event: any) {
    const modalRef = this.modal.open(ActionRoomComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance.action = true;
    modalRef.result.then(value => {
        if (value) {
          this.getAll();
        }
      },
    );
  }

}
