import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Rooms } from './Rooms';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {

  API_BASE_URL = 'http://localhost:8089/rooms';

  constructor(
    private http : HttpClient
  ) { }

  public getALlRoom():Observable<any>{
    return this.http.get<any>(this.API_BASE_URL + '/findAllRoom');
  }

  public addRoom(room: Rooms):Observable<any>{
    return this.http.post<any>(this.API_BASE_URL + '/addRoom', room);
  }

  public editRoom(room: Rooms, id: number):Observable<any>{
    return this.http.post<any>(this.API_BASE_URL + '/editRoom/' + id , room);
  }
}
