import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomsComponent } from './rooms.component';
import { RouterModule, Routes } from '@angular/router';
import { DetailRoomComponent } from './detail-room/detail-room.component';
import { ActionRoomComponent } from './action-room/action-room.component';
import { SharesModule } from '../../shares/shares.module';
import { MaterialModule } from '../../shares/material.module';
import { PrimengModule } from '../../shares/primeng.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmationService, MessageService } from 'primeng/api';

const routes: Routes = [
  {
    path: '',
    component: RoomsComponent,
    children: [
      {
        path:'detail-room',
        component:DetailRoomComponent
      }
    ]
  }
];

@NgModule({
  declarations: [RoomsComponent, DetailRoomComponent, ActionRoomComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharesModule,
    MaterialModule,
    PrimengModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [MessageService, ConfirmationService],
})
export class RoomsModule { }
