import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { RoomsService } from '../rooms.service';

class PagedData<T> {
  data: T[];
}

@Component({
  selector: 'ngx-action-room',
  templateUrl: './action-room.component.html',
  styleUrls: ['./action-room.component.scss']
})
export class ActionRoomComponent implements OnInit {

  @Input() action: any;
  @Input() room: any;
  isSubmitted: boolean = false;
  formRoom: FormGroup;
  listTeacher: any[] = [];

  constructor(
    private  roomService: RoomsService,
    private modal: NgbActiveModal,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    if (this.action) {
      this.formRoom = this.fb.group({
        nameRoom: ['', Validators.required],
        headTeacher: ['', Validators.required],
        currentSize: ['', Validators.required, Validators.min(1), Validators.max(50)],
        maxSize: ['', Validators.required, Validators.min(1), Validators.max(50)],
        startCreated: ['', Validators.required],
      });
    } else {
      this.formRoom = this.fb.group({
        id: [this.room.id],
        nameRoom: [this.room.nameRoom, Validators.required],
        headTeacher: [this.room.headTeacher, Validators.required],
        currentSize: [this.room.currentSize, Validators.required],
        maxSize: [this.room.maxSize, Validators.required],
        startCreated: [this.room.startCreated, Validators.required],
      });
    }
  }

  close(status = true) {
    this.modal.close(status);
  }

  get f() {
    return this.formRoom.controls;
  }

  processSaveOrUpdate() {
    this.isSubmitted = true;
    if (this.formRoom.valid) {
      this.spinner.show();
      if (this.action) {
        this.roomService.addRoom(this.formRoom.value).subscribe(
          data => {
            this.toastr.success('Thêm mới thành công');
            this.close();
          });
      }
      else {
        this.roomService.editRoom(this.formRoom.value, this.room.id).subscribe(
          data => {
          this.toastr.success('Cập nhật thành công !');
          this.close();
        })
      }
    }
  }

}
