import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { TeachersService } from '../teachers.service';

class PagedData<T> {
  data: T[];
}

@Component({
  selector: 'ngx-action-teacher',
  templateUrl: './action-teacher.component.html',
  styleUrls: ['./action-teacher.component.scss']
})
export class ActionTeacherComponent implements OnInit {

  @Input() action: any;
  @Input() teacher: any;
  isSubmitted: boolean = false;
  formTeacher: FormGroup;
  listTeacher: any[] = [];

  constructor(
    private modal: NgbActiveModal,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private teacherService : TeachersService
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    if (this.action) {
      this.formTeacher = this.fb.group({
        teacherName: ['', Validators.required],
        email: ['', Validators.required, Validators.email],
        phoneNumber: ['', Validators.required, Validators.pattern("(0)[0-9 ]{10}")],
        address: ['', Validators.required],
        gender: ['', Validators.required],
      });
    } else {
      this.formTeacher = this.fb.group({
        id: [this.teacher.id],
        teacherName: [this.teacher.teacherName, Validators.required],
        email: [this.teacher.email, Validators.required],
        phoneNumber: [this.teacher.phoneNumber, Validators.required],
        address: [this.teacher.address, Validators.required],
        gender: [this.teacher.gender, Validators.required],
      });
    }
  }

  close(status = true) {
    this.modal.close(status);
  }

  get f() {
    return this.formTeacher.controls;
  }

  processSaveOrUpdate() {
    this.isSubmitted = true;
    if (this.formTeacher.valid) {
      this.spinner.show();
      if (this.action) {
        this.teacherService.creatTeacher(this.formTeacher.value).subscribe(
          data => {
            this.toastr.success('Thêm mới thành công');
            this.close();
          });
      }
      else {
        this.teacherService.editTeacher(this.formTeacher.value, this.teacher.id).subscribe(
          data => {
          this.toastr.success('Cập nhật thành công !');
          this.close();
        });
      }
    }
  }

}
