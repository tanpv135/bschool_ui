import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeachersComponent } from './teachers.component';
import { DetailTeacherComponent } from './detail-teacher/detail-teacher.component';
import { ActionTeacherComponent } from './action-teacher/action-teacher.component';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '../../shares/material.module';
import { PrimengModule } from '../../shares/primeng.module';
import { MessageService, ConfirmationService } from 'primeng/api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SharesModule } from '../../shares/shares.module';

const routes: Routes = [
  {
    path:'',
    component: TeachersComponent,
    children:[
      {
        path:'detail-teacher',
        children:[
          {
            path:'id',
            component:DetailTeacherComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  declarations: [TeachersComponent, DetailTeacherComponent, ActionTeacherComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    PrimengModule,
    SharesModule,
    NgxDatatableModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [MessageService, ConfirmationService],
})
export class TeachersModule { }
