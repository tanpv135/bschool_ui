import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Teachers } from '../../@core/model/Teachers';

@Injectable({
  providedIn: 'root'
})
export class TeachersService {

  API_BASE_URL = 'http://localhost:8089/teacher';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    'Access-Control-Allow-Origin': 'http://localhost:4200',
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
  };

  constructor(
    private http : HttpClient
    ) { }

    public getAllTeacher():Observable<any>{
      return this.http.get<any>(this.API_BASE_URL + '/getAll');
    }

    public searchTeacher(teacher:Teachers):Observable<any>{
      return this.http.post<any>(this.API_BASE_URL + '/getListTeacher' , teacher);
    }

    public deleteTeacher(id:number):Observable<any>{
      return this.http.post<any>(this.API_BASE_URL + '/deleteTeacher/' + id,this.httpOptions)
    }

    public creatTeacher(teacher: Teachers):Observable<any>{
      return this.http.post<any>(this.API_BASE_URL + '/addTeacher', teacher);
    }

    public editTeacher(teacher: Teachers, id: number):Observable<any>{
      return this.http.post<any>(this.API_BASE_URL + '/editTeacher/' + id , teacher);
    }

}
