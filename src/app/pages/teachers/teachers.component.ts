import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { debug } from "console";
import { ToastrService } from "ngx-toastr";
import { ConfirmationService, MessageService, PrimeNGConfig } from "primeng/api";
import { threadId } from "worker_threads";
import { DEFAULT_MODAL_OPTIONS } from "../../@core/app-config";
import { SearchTeacher } from "../../@core/model/SearchTeacher";
import { Teachers } from "../../@core/model/Teachers";
import { Rooms } from "../rooms/Rooms";
import { RoomsService } from "../rooms/rooms.service";
import { ActionTeacherComponent } from "./action-teacher/action-teacher.component";
import { TeachersService } from "./teachers.service";

class PagedData<T> {
  data: T[];
}

@Component({
  selector: "ngx-teachers",
  templateUrl: "./teachers.component.html",
  styleUrls: ["./teachers.component.scss"],
})
export class TeachersComponent implements OnInit {
  teacherDialog: boolean;

  teachers: Teachers[] = [];
  rooms : Rooms[];

  teacherFrom:FormGroup;

  teacherName = '';
  email = '';
  address = '';
  phoneNumber = '';
  nameRoom = '';

  teacher: Teachers = new Teachers();

  selectedTeachers: Teachers[];

  submitted: boolean;


  constructor(
    private teachserService: TeachersService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private fb : FormBuilder,
    private primengConfig: PrimeNGConfig,
    private toastr: ToastrService,
    private modal: NgbModal,
    private roomService:RoomsService
  ) {}

  initForm(){
    this.teacherFrom = this.fb.group({
      teacherName: [''],
      email:[''],
      phoneNumber:[''],
      address:[''],
      nameRoom:['']
    })
  }

  get f(){
   return this.teacherFrom.controls;
  }

  ngOnInit(): void {
    this.getAllTeacher();
    this.primengConfig.ripple = true;
    this. getAllRoom();
    this.initForm();
  }

  public getAllTeacher(): void {
    this.teachserService.getAllTeacher().subscribe(
      (data) => {
        this.teachers = data;
      }, error => console.log(error)
    )
  }

  getAllRoom(){
    this.roomService.getALlRoom().subscribe(
      data =>{
        this.rooms = data;
        console.log(this.rooms);

      }
    )
  }

  search(){
    this.teachserService.searchTeacher(this.teacherFrom.value).subscribe(
      data =>{
        this.teachers = data.data;
        console.log("list ne",this.teachers);

      }, error => console.log(error)
    )
  }

  // retrieveTeachers():void{
  //   const params = this.getRequestParams(this.teacherName, this.email,  this.phoneNumber, this.address, this.nameRoom);
  //   this.teachserService.searchTeacher(params).subscribe(
  //     data =>{
  //       this.teachers = data.data;
  //       console.log(data);
  //     }
  //     ,error => {console.log(error)}
  //   );
  // }

  // search(){
  //   this.retrieveTeachers();
  // }

  deleteTeacher(teacher: Teachers) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + teacher.teacherName + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.teachserService.deleteTeacher(teacher.id).subscribe(
          data => {
          if (teacher.deleteTeacher === false) {
            this.toastr.warning("Deleted table failed!")
          } else {
            this.toastr.success("Deleted table successfully!")
            this.getAllTeacher();
          }
        })
      }
    });
  }

  processEdit(teacher: any) {
    const modalRef = this.modal.open(ActionTeacherComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance.action = false;
    modalRef.componentInstance.teacher = teacher;
    modalRef.result.then(value => {
        if (value) {
          this.getAllTeacher();
        }
      },
    );
  }

  processSave($event: any) {
    const modalRef = this.modal.open(ActionTeacherComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance.action = true;
    modalRef.result.then(value => {
        if (value) {
          this.getAllTeacher();
        }
      },
    );
  }


}
