import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentsComponent } from './students.component';
import { DetailStudentComponent } from './detail-student/detail-student.component';
import { ActionStudentComponent } from './action-student/action-student.component';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '../../shares/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PrimengModule } from '../../shares/primeng.module';
import { SharesModule } from '../../shares/shares.module';
import { ConfirmationService, MessageService } from 'primeng/api';

const routes: Routes = [
  {
    path:'',
    component:StudentsComponent,
    children:[
      {
        path:'detail-student',
        component:DetailStudentComponent
      }
    ]
  }
]

@NgModule({
  declarations: [StudentsComponent, DetailStudentComponent, ActionStudentComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    PrimengModule,
    SharesModule,
    NgxDatatableModule,
  ],
  providers: [MessageService, ConfirmationService],
})
export class StudentsModule { }
