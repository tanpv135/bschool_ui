import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Students } from './Students';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  API_BASE_URL = 'http://localhost:8089/student';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    'Access-Control-Allow-Origin': 'http://localhost:4200',
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
  };

  constructor(
    private http: HttpClient
  ) { }

  public getAllStudent():Observable<any>{
    return this.http.get<any>(this.API_BASE_URL + '/getAllStudent');
  }

  public deleteStudent(id:number):Observable<any>{
    return this.http.post<any>(this.API_BASE_URL + '/deleteStudent/' + id, this.httpOptions);
  }

  public createStudent(student:Students){
    return this.http.post<any>(this.API_BASE_URL + '/addStudent' , student);
  }

  editStudent(id:number, student: Students){
    return this.http.post<any>(this.API_BASE_URL + '/editStudent/' + id , student);
  }
}
