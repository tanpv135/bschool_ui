import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { StudentsService } from '../students.service';

class PagedData<T> {
  data: T[];
}

@Component({
  selector: 'ngx-action-student',
  templateUrl: './action-student.component.html',
  styleUrls: ['./action-student.component.scss']
})
export class ActionStudentComponent implements OnInit {

  @Input() action: any;
  @Input() student: any;
  isSubmitted: boolean = false;
  formStudent: FormGroup;
  listStudent: any[] = [];

  constructor(
    private modal: NgbActiveModal,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private studentService : StudentsService
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    if (this.action) {
      this.formStudent = this.fb.group({
        studentName: ['', Validators.required],
        email: ['', Validators.required, Validators.email],
        phoneNumber: ['', Validators.required, Validators.pattern("(0)[0-9 ]{10}")],
        address: ['', Validators.required],
        gender: ['', Validators.required],
      });
    } else {
      this.formStudent = this.fb.group({
        id: [this.student.id],
        studentName: [this.student.studentName, Validators.required],
        email: [this.student.email, Validators.required],
        phoneNumber: [this.student.phoneNumber, Validators.required],
        address: [this.student.address, Validators.required],
        gender: [this.student.gender, Validators.required],
      });
    }
  }

  close(status = true) {
    this.modal.close(status);
  }

  get f() {
    return this.formStudent.controls;
  }

  processSaveOrUpdate() {
    this.isSubmitted = true;
    if (this.formStudent.valid) {
      this.spinner.show();
      if (this.action) {
        this.studentService.createStudent(this.formStudent.value).subscribe(
          data =>{
            this.toastr.success('Thêm mới thành công')
          }
        )}
      else {
        this.studentService.editStudent(this.formStudent.value, this.student.id).subscribe(data => {
          this.toastr.success('Cập nhật thành công !');
          this.close();
        })
      }
    }
  }

}
