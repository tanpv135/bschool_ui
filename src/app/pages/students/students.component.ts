import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService, MessageService, PrimeNGConfig } from 'primeng/api';
import { DEFAULT_MODAL_OPTIONS } from '../../@core/app-config';
import { Students } from '../../@core/model/Students';
import { StudentSearch } from '../../@core/model/StudentSearch';
import { ActionStudentComponent } from './action-student/action-student.component';
import { StudentsService } from './students.service';

class PagedData<T> {
  data: T[];
}

@Component({
  selector: 'ngx-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {

  studentDialog:boolean;

  students: Students[];


  student: Students;

  selectedStudents: Students[];

  submitted: boolean;



  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private fb : FormBuilder,
    private primengConfig: PrimeNGConfig,
    private toastr: ToastrService,
    private modal: NgbModal,
    private studentService: StudentsService
  ) { }

  ngOnInit(): void {
    this.getAllStudent();
    this.primengConfig.ripple = true;
  }

  getAllStudent(){
    this.studentService.getAllStudent().subscribe(data =>{
      this.students = data;
    }, error => console.log(error));
  }

  deleteStudent(student: Students) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + student.studentName + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.studentService.deleteStudent(student.id).subscribe(
          data => {
          if (student.deleteStudent === false) {
            this.toastr.warning("Deleted table failed!")
          } else {
            this.toastr.success("Deleted table successfully!")
            this.getAllStudent();
          }
        })
      }
    });
  }

  processEdit(student: any) {
    const modalRef = this.modal.open(ActionStudentComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance.action = false;
    modalRef.componentInstance.student = student;
    modalRef.result.then(value => {
        if (value) {
          this.getAllStudent();
        }
      },
    );
  }

  processSave($event: any) {
    const modalRef = this.modal.open(ActionStudentComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance.action = true;
    modalRef.result.then(value => {
        if (value) {
          this.getAllStudent();
        }
      },
    );
  }

  search(){

  }

  addStudent(){}

}
