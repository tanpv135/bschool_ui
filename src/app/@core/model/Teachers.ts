export class Teachers{
  id?:any;
  teacherName?:any;
  teacherType?:any;
  email?:any;
  phoneNumber?:any;
  address?:any;
  dob?:any;
  gender?:any;
  startYear?:any;
  endYear?:any;
  status?:any;
  deleteTeacher?:Boolean;
  nameRoom:any;
}
