export interface Students{
  id:any;
  studentName:any;
  email:any;
  address:any;
  dob:any;
  startYear:any;
  endYear:any;
  status:any;
  gender:any;
  studentType:any;
  phoneNumber:any;
  deleteStudent:any;
}
